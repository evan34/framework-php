<?php


abstract class Controller extends DAOConcret
{
    /**
     * Valeur passée dans l'url en get
     *
     * @var array
     */
    private $get;

    /**
     * Valeur passée dans l'url en post
     *
     * @var array
     */
    private $post;

    /**
     * Mise à jour de ressource
     *
     * @var array
     */
    private $put;

    /**
     * Cette propriété sera initialisée lors de l'appel de la methode 
     * securityLoader()
     * @var SecurityMiddleware
     */
    protected $security;

    /**
     * Initialise le contexte de security.
     */
    protected function securityLoader() {
        $this->security = new SecurityMiddleware();
    }


    /**
     * Initialise les propriétés avec les valeurs des superglobales
     */
    public function __construct() 
    {
        $this->get = $_GET;
        $this->post = $_POST;
    }

    /**
     * getter en lecture seule de $_GET
     *
     * @return array
     */
    protected function inputGet() 
    {
        return $this->get;
    }

    /**
     * Ugetter en lecture seule de $_POST
     *
     * @return array
     */
    protected function inputPost() 
    {
        return $this->post;
    }

     /**
     * retourne la propriété $put afin de la rendre disponible aux developpeurs
     * souhaitant étendre cette classe
     * 
     * @return array
     */
    protected function inputPut() {
        return $this->put;
    }

    /**
     * Prend deux arguments, le chemin du fichier de vue et les données à injecter dans la vue
     *
     * @param string $pathToView
     * @param array $datas
     * 
     */
    final protected function render($pathToView, $datas =null) 
    {
        $user = null;
        if(!is_null($this->security)){
            $user = $this->security->connexion();
            $user = (!$user)?null:$user;
        }        
        if(is_array($datas)){
            foreach ($datas as $key => $value) {
                $key = $value;
            }
        }
        require ('./views/'. $pathToView . '.php');
    }
}