<?php

interface CRUDInterface 
{
    public function retrieve($id);
    public function update($Array);
    public function delete($id);
    public function create($Array);
}