<?php

//Create the Abstract class for the DAO
 abstract class DAO implements CRUDInterface,RepositoryInterface {

    private $pdo;

    function __construct(){
        $config = json_decode(file_get_contents('./config/database.json'),true);
        
        try{
            $this->pdo = new PDO(
                $config['driver'] 
                . ':dbname=' . $config['dbname'] 
                . ';host='.$config['host']
                . ';port=' .$config['port']
                . ';charset=utf8', 
                $config['username'], 
                $config['password']
            );
        }

        catch( PDOException $Exception  ) {
            
            return "some fail-messages";

        }
    }

    protected function getPdo() 
    {
        return $this->pdo;
    }

    public function retrieve($id){
    }

    public function update($id){
    }

    public function delete($id){
    }

    public function create($assocArray){
    }

    public function getAll(){
    }
}