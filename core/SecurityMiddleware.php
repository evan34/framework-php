<?php


class SecurityMiddleware {

    private $token;

    private $key = "the_key";

    private $expiration = (60*60*24);


    public function __construct () 
    {
        
    }

    public function generateToken ()
    {
        $this->token = array(
        "username" => "evan",
        "password" => "bla",
        "exp" => time() + $this->expiration
        );

        $tkn = JWT::encode($this->token, $this->key);
        setcookie("tkn", $tkn, $this->token["exp"], "http://" . $_SERVER['SERVER_NAME']);
        var_dump($tkn);
        return $tkn;
        
        

    }

    public function verifyToken($jwt) 
    {
        try {
            return JWT::decode($jwt, $this->key, array('HS256'));
        
        } catch (Exception $exception){
            return false;
        }
    }

    public function connexion() 
    {
        $tkn = (isset($_COOKIE['tkn'])) ? $_COOKIE['tkn'] : null;
        return $this->verifyToken($tkn);
    }

}

