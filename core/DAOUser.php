<?php

include 'DAO.php';


    class DaoConcret extends DAO {
    
    
        protected $pdo;
    
    
    
        public function __construct(){
            $config = json_decode(file_get_contents('./config/database.json'),true);
            
            //$this->pdo= new PDO('mysql:dbname=test_base;host=localhost','phpmyadmin','root');
    
            try{
                $options = 
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false
                ];
    
                $this->pdo = new PDO($config['driver'] . ':dbname=' 
                                   . $config['dbname'] . ';host='
                                   . $config['host'],
                                     $config['username'], 
                                     $config['password']
                                    );
                                }
                
            catch( PDOException $Exception  ) {
                
                echo 'ERROR : ' . $Exception->getMessage();
    
            }
        }
    
    
    
    
    // Methode DAO to read
        public function retrieve($id){
    
            $sql = 'SELECT id, brand, vehicle FROM thecar WHERE id ='.$id;   
            $db = $this->pdo; 
            $response = $db->prepare($sql);
            $response->execute([$id]);
            $list = $response->fetch(PDO::FETCH_ASSOC); 
            
            echo '<p>' . $list['id'] . ' - ' . $list['brand'] . ' : ' . $list['vehicle'] . '</p>';       
        }
    
    
    // Methode DAO to update
        public function update($id){
    
        
    
        }
    
    // Methode DAO to delete
        public function delete($id){
    
    
        }
    
    // Methode DAO to Create
        public function create($assocArray){
    
    
        }
    
    //Methode to get all the datas 
    
    public function getAll(){
        
        $maRequete= 'SELECT * FROM thecar';
        $db =  $this->pdo;
        $response = $db -> query($maRequete);
        $res = $response->fetchAll();
        //var_dump($res);
        return $res;
        
    }
    
    }


   