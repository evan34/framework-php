<?php

class Routing {

    /**
     * Fichier de configuration routing.json
     * @var array
     */  
    private $config; 
    
    /**
     * Tableau: URI
     * @var array
     */
    private  $uri; 
    
    /**
     * Tableau: route de l'URI
     * @var array
     */
    private  $route; 
    
    /**
     * Contrôleur qui a été trouvé
     * @var string
     */
    private  $controller; 
    
    /**
     * Tableau des arguments à passer au contrôleur, 
     * qui représente les éléments variables de l’URI
     * @var array
     */
    private  $args; 
    
    /**
     * Verbe http utilisé lors de la requete
     * @var string
     */
    private  $method; 


    public function __construct() {
       
    }

    /**
     * Déclenche le mécanisme de routage à chaque requête http
     * @return void
     */
    public function execute()
    {

        $this->uri = explode('/',$_SERVER['REQUEST_URI']);
            
        $this->config = json_decode(file_get_contents('./config/routing.json'), true);
        
        $this->route = array_keys($this->config);
        
        $maVar = $this->route;

        for ($j = 0; $j < count($maVar); $j++) {
            $this->route2 = explode('/',$this->route[$j]);               
            $this->isEqual($this->uri, $this->route2);            
        }      
    }

    /**
     * Retourne un booléen correspondant au résultat de la comparaison des hauteurs des deux tableaux.
     * @return boolean
     */
    public function isEqual($array1,$array2)
    { 
        /*
        ici on affecte a la propriété args les différences entre les deux tableaux
        et ensuite on remplace 
        */
        $this->args = array_diff($array1,$array2);
        $array1 = str_replace($this->args,"(:)",$array1); // ici j'ai changé quelque chose        
        if($array1===$array2){
           $this->getValue($array1,$array2); 
        }          
    }

    /**
     * Retourne le contrôleur correspondant à la route sélectionnée
     * @return any
     */
    public function getValue($array1,$array2)
    {
        $road = implode('/',$this->route2);
        $value= $this->config[$road];
        var_dump($value);

        if (gettype($value)==='string'){
            $result = explode(':',$value);
            $this->controller = $result;
            $this->invoke();
        }else{

            $method = $_SERVER['REQUEST_METHOD'];
            var_dump($method);
            foreach ($value as $key => $val) {
                if($key===$method){                
                    $result = explode(":",$val);
                    $this->controller = $result;
                    $this->addArgument();                  
                    $this->invoke();
                }
            }
        }
    }

    /**
     * Ajoute l’élément variable de l’URI si l’élément en cours est censé être variable
     * @var int
     */
    public function addArgument()
    {
        $res_uri = end($this->uri);
        $route = $this->route;

        for($i=0;$i<sizeof($this->route);$i++){
            $res =$route[$i];
            $this->args = substr(strrchr($res,"/"),1);
            if($this->args==="(:)"){
                $this->args=str_replace($this->args,$res_uri,"(:)");
            }
        }        
    }

     /**
     * Compare les éléments des deux tableaux
     * @var string
     */
    public function compare()
    {

        
    }

    /**
     * Créée un objet contrôleur et invoque la méthode en y passant les arguments
     * @param
     */
    public function  invoke()
    {
      $controller = $this->controller;
      $this->sanitize();  
      $Controller = $this->controller;    
      $objTest = new $controller[0]();      
      $methode = $Controller[1];
      $objTest->$methode($this->args);         
    }

    /**
     * Nettoie les listes $uri et $route en enlevant les éléments qui sont une chaine vide
     *
     * @return array
     */
    public function sanitize() {
      $controller = $this->controller;
      $uri = $this->uri;
     // var_dump($controller);
      foreach ($controller as $k=>$v) {
        if ($v == "") {
          unset($controller[$k]);
        }
        $this->controller = array_values($controller);
        
      }
      foreach ($uri as $k=>$v) {
        if ($v == "") {
          unset($uri[$k]);
        }
        $uri = array_values($uri);
      }
    }
  
    

}

    





