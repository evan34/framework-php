<?php
use Whoops\Exception\ErrorException;


class RoutingTest 
{
    /**
     * Mapping du fichier routing.json
     *
     * @var array
     */  
    private $config;

    /**
     * Résultat du découpage de l’URI en tableau
     *
     * @var array
     */
    private $uri;

    /**
     * Découpage de la route (clé de config) testée
     *
     * @var array
     */
    private $route;
  
    /**
     * Contrôleur qui a été trouvé
     *
     * @var string
     */
    private $controller;

    /**
     * Tableau des arguments à passer au contrôleur, qui représente les éléments variables de l’URI
     *
     * @var array
     */
    private $args;

    /**
     * Verbe http utilisé lors de la requete
     *
     * @var string
     */
    private $method;


    public function __construct()   
    {
               
    }

    /**
     * Déclenche le mécanisme de routage à chaque requête http
     *
     * @return void
     */
    public function execute() 
    {   
        //Convertis et place dans un tableau $this->route le fichier routing.json
        $this->config = json_decode(file_get_contents('./config/routing.json'), true);
        
        $this->route = array_keys($this->config);

        for ($i = 0; $i < sizeof($this->route); $i++) {
            $routeItems =  explode('/', $this->route[$i]);          

        }
        

        //Convertis et place dans un tableau l'uri
        $this->uri = parse_url("http://localhost/api/users/2", PHP_URL_PATH);
        $this->uri = explode('/', $_SERVER['REQUEST_URI']);
        
        //On compare la hauteur des deux tableaux
            if (array_intersect_key($this->uri, $routeItems) == true) {
                $this->compare($this->uri, $routeItems);
                $this->isEqual($this->uri);
            } else {
                throw new ErrorException('Les tableaux ne sont pas de même longueur !');             
            }           
    }

    /**
     * Retourne un booléen correspondant au résultat de la comparaison des hauteurs des deux tableaux.
     *
     * @return boolean
     */
    public function isEqual() 
    {
        return (count($this->uri) === count($this->route)) ? true : false;
    }

    /**
     * Retourne le contrôleur correspondant à la route sélectionnée
     *
     * @return any
     */
    public function getValue() 
    {
        $road = implode('/',$this->route2);
        $value= $this->config[$road];
            if (gettype($value)==='string'){
                $result = explode(':',$value);
                $this->controller = $result;          
                $this->invoke();
            }else{
                $method = $_SERVER['REQUEST_METHOD'];
                foreach ($value as $key => $val) {
                    if($key===$method){                
                        $result = explode(":",$val);
                        $this->controller = $result;
                        $this->addArgument();                   
                        $this->invoke();    
                    }
                }
            } 
    }

    /**
     * Ajoute l’élément variable de l’URI si l’élément en cours est censé être variable
     *
     * @var int
     */
    public function addArgument() 
    {
      $uri = $this->uri;
      $res_uri = end($uri);
      $route = $this->route;
      for ($i = 0; $i < sizeof($this->route); $i++) {
          $res = $route[$i];         
          $this->args = substr(strrchr($res, "/"), 1);
            if ($this->args === '(:)') {
              $this->args = str_replace('(:)', $this->args, $res_uri);
            }   
      }    
      $this->getValue($this->args);
    }

    /**
     * Compare les éléments des deux tableaux
     *
     * @var string
     */
    public function compare() 
    {
        $uri = implode('/', $this->uri);
        foreach ($this->route as $key=>$value) {            
            if($value == $uri) {
                $result = $value;
            } elseif ($value !== $uri) {
                //echo 'les tableaux sont différents mais on va vérifier l\'élément variable';
                $result = $value;
                $result = explode('/', $value);
                if (in_array('(:)', $result)) {
                    $this->addArgument();
                }
            }
        }
    }

    /**
     * Créée un objet contrôleur et invoque la méthode en y passant les arguments
     *
     * @param
     */
    public function invoke() 
    {
      $result = $this->controller;
      
      $args = $this->args;
     
    }
}