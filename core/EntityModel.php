<?php 

abstract class EntityModel implements Persistable
{
    protected $dao;

    public function __construct() {

    }

    public function load($id) {
        $this->dao->retrieve($id); 
    }

    public function update($id) {
        $id = get_class($this);
        if ($id == null) {
            $this->dao->create($id);
        } else {
            $this->dao->update($id);
        }
         
    }

    public function remove($id) {
        $this->dao->delete($id); 
    }
}